# topdesk.py

This is a - work in progress - Python 3 wrapper for the [TOPdesk
API](https://developers.topdesk.com/). The wrapper currently focuses on exposing
the endpoints related to asset management.

Install this package by running:

```bash
pip3 install --force-reinstall git+https://gitlab.com/naturalis/MII/topdesk.py
```
