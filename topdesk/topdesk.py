import requests
import json
import logging

FORMAT = u'%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger('topdesk')
logger.setLevel(logging.WARNING)

class Topdesk():

    def __init__(self, baseurl, user, secret):
        self.baseurl = baseurl
        self.login(user=user, secret=secret)

    def login(self, user, secret):
        self.session = requests.Session()
        endpoint = '/tas/api/login/operator'
        url = self.baseurl + endpoint
        r = self.session.get(url, auth=(user, secret))
        self.tokenid = r.content.decode('utf-8')
        headers = {
                'accept' : 'application/json',
                'content-type' : 'application/json',
                'Authorization': 'TOKEN id=\"' + self.tokenid + '\"',
                'cache-control': 'no-cache'
        }
        self.session.headers.update(headers)
        Topdesk.session = self.session
        logger.debug("Login success, tokenid=%s", self.tokenid)


    def logout(self):
        self.session = requests.Session()
        endpoint = '/tas/api/logout'
        url = self.baseurl + endpoint
        r = self.session.get(url)
        if r:
            self.session = None
            self.token = None

    def request(self, endpoint, method='get', payload=None):
        url = self.baseurl + endpoint
        if not self.session:
            self.session = requests.Session()
        if method == 'post':
            jsonData = json.dumps(payload)
            logger.debug("HTTP POST to '%s'", url)
            return self.session.post(url, data=jsonData)
        if method == 'delete':
            logger.debug("HTTP DELETE to '%s'", url)
            return self.session.delete(url)
        else:
            if payload:
                logger.debug("HTTP GET to '%s'", url)
                return self.session.get(url, params=payload)
            else:
                logger.debug("HTTP GET to '%s'", url)
                return self.session.get(url)

    def getAsset(self, unid=None, name=None):
        if unid:
            a = Asset(self, unid=unid)
        elif name:
            a = Asset(self, name=name)
        if a.unid:
            logger.debug("Asset found %s", a.unid)
            return a
        logger.debug("Could not retrieve asset")

    def findAssets(self, payload={}):
        results = []
        asseturl = '/tas/api/assetmgmt/assets'
        try :
            request = self.request(asseturl, 'get', payload)
            assets = json.loads(request.content.decode('utf-8'))
            dataset = assets.get('dataSet', [])
            if (payload.get('$filter') and payload.get('$filter').startswith('name gt')):
                while (len(dataset)):
                    results = results + dataset

                    last = dataset[-1]
                    payload['$filter'] = "name gt '%s'" % (last.get('text'))
                    request = self.request(asseturl, 'get', payload)
                    assets = json.loads(request.content.decode('utf-8'))
                    dataset = assets.get('dataSet', [])
            else:
                results = results + dataset
            
        except Exception as e:
            logger.error('findAssets request failed, asset not found?')
            logger.error(e)

        return results

    def listTemplates(self):
        endpoint = "/tas/api/assetmgmt/templates"
        request = self.request(endpoint, 'get')
        result = json.loads(request.content.decode('utf-8'))
        return result.get('dataSet', [])

    def createAsset(self, templateName=None):
        asset = Asset(self, templateName=templateName)
        return asset

    def getTemplateId(self, templateName=None):
        templates = self.listTemplates()
        for template in templates:
            if template.get('text').lower() == templateName.lower():
                return template.get('id')

        return None

    def listAssets(self, templateId=None, fields=None):
        if templateId:
            endpoint = "/tas/api/assetmgmt/assets/templateId/%s" % (templateId)
            if fields:
                endpoint += '?field=' + fields

            request = self.request(endpoint, 'get')
            result = json.loads(request.content.decode('utf-8'))

            return result.get('results')

    def listRelationTypes(self, sourceId=None, targetId=None):
        if sourceId and targetId:
            endpoint = "/tas/api/assetmgmt/assetLinks/possibleRelations"
            endpoint += '?sourceId={sourceId}&targetId={targetId}'.format(sourceId=sourceId, targetId=targetId)

            request = self.request(endpoint, 'get')
            result = json.loads(request.content.decode('utf-8'))

            return result

    def listCapabilities(self):
        endpoint = '/tas/api/assetmgmt/capabilities'
        response = self.request(endpoint, 'get')
        if response.ok:
            capabilities = json.loads(response.content.decode('utf-8'))
            return capabilities['dataSet']
        else:
            logger.error("request %d error: '%s'",
                         response.status_code,
                         response.content.decode('utf-8'))
            return None

    def listAssetLinks(self, unid=None):
        endpoint = '/tas/api/assetmgmt/assetLinks'
        payload = {"sourceId": unid}
        response = self.request(endpoint, 'get', payload=payload)
        if response.ok:
            assetlinks = json.loads(response.content.decode('utf-8'))
            return assetlinks
        else:
            logger.error("request %d error: '%s'",
                         response.status_code,
                         response.content.decode('utf-8'))
            return None

    def createAssetLink(self, sourceid=None, targetid=None,
                        linktype=None, capabilityid=None):
        a = AssetLink(self, sourceid=sourceid,
                      targetid=targetid,
                      linktype=linktype,
                      capabilityid=capabilityid)
        a.create()
        return a

    def deleteAssetLink(self, sourceid=None, targetid=None,
                        linktype=None, capabilityid=None, linkid=None):
        a = AssetLink(self, sourceid=sourceid,
                      targetid=targetid,
                      linktype=linktype,
                      linkid=linkid,
                      capabilityid=capabilityid)
        a.delete()
        return None

class Asset():
    def __init__(self, topdesk=None, unid=None, name=None, templateName=None):
        self.topdesk = topdesk

        if unid:
            try:
                self.unid = unid.decode('utf-8')
            except:
                self.unid = unid
        elif name:
            payload = {'$filter': 'name eq ' + name}
            assets = self.topdesk.findAssets(payload=payload)
            if (len(assets) == 0):
                return
            self.unid = assets[0]['id']
        elif templateName:
            templateId = self.topdesk.getTemplateId(templateName)
            if templateId:
                endpoint = '/tas/api/assetmgmt/assets/blank'
                payload = {'templateId' : templateId}
                response = self.topdesk.request(endpoint, 'get', payload=payload)
                if response.ok:
                    result = json.loads(response.content.decode('utf-8'))
                    self.data = result
                    self.template_id = self.data['data']['type_id']
                    self.name = self.data['data']['name']
                    self.newdata = {}
                else:
                    logger.error("request %d error: '%s'", response.status_code,
                                 response.content.decode('utf-8'))
            else:
                logger.error("template '%s' does not exist", templateName)
            return
        else:
            return

        if self.unid:
            endpoint = '/tas/api/assetmgmt/assets/' + self.unid
            response = self.topdesk.request(endpoint)
            self.data = json.loads(response.content.decode('utf-8'))

            # Loop to make all keys and values instance attributes
            # for k, v in data['data'].items():
            #    setattr(self, k, v)
            self.name = self.data['data']['name']
            self.status = self.data['data']['@status']
            self.archived = self.data['data']['archived']
            self.template_id = self.data['data']['type_id']
            self.newdata = {}

    def dropdown_values(self, name='', value=''):
        """
        Retrieves the various values of type 'suggestField'. Using endpoint:

        /tas/api/assetmgmt/[url]
        """
        values = False
        if self.data['fields'].get(name):
            field = self.data['fields'][name]
            if field['fieldType'] == 'suggestField':
                if field['properties'].get('templateId'):
                    # When the field is list of assets in assetmanagement
                    endpoint = '/tas/api/assetmgmt/dropdowns/' + field['properties'].get('templateId') + '?field=name'
                else:
                    # The field is a list of something else of the main
                    # topdesk api which can be: suppliers, budgetholders, countries,
                    # departments, locations, languages, operatorgroups,
                    # operators, persongroups, persons, supplierContacts and
                    # suppliers
                    #
                    # For now, only suppliers are supported
                    #
                    dataset = field['properties'].get('dataset')
                    if dataset == 'supplier':
                        endpoint = '/tas/api/suppliers'
                        if value:
                            endpoint = endpoint + '?name=' + value
                    else:
                        logger.error("field '%s' is not supported yet",
                                     dataset)
                        return False
                results = False
                try:
                    response = self.topdesk.request(endpoint)
                    results = json.loads(response.content.decode('utf-8'))
                    if results.get('results', False):
                        values = {}
                        results = jsondata.get('results')
                except:
                    pass

                if isinstance(results, list):
                    values = {}
                    for option in results:
                        values[option['id']] = option.get('name', option['id'])
        return values

    def dropdown_value(self, name='', key=''):
        values = self.dropdown_values(name)
        if not values:
            logger.error("Value '{value}' does not exist in field '{name}'".format(
                name=name,
                value=value
            ))
            return False

        return values.get(key, '')

    def dropdown_key(self, name='', value=''):
        values = self.dropdown_values(name, value)
        if not values:
            logger.error("Value '{value}' does not exist in field '{name}'".format(
                name=name,
                value=value
            ))
            return False

        inv_values = {v.lower(): k for k, v in values.items()}
        return inv_values.get(value.lower(), False)


    def save(self):
        payload = self.newdata
        endpoint = '/tas/api/assetmgmt/assets'

        try:
            endpoint = endpoint + '/' + self.unid
        except:
            pass
        response = self.topdesk.request(endpoint, 'post', payload)

        if response.ok:
            return
        else:
            logger.error("request %d error: '%s'",
                         response.status_code,
                         response.content.decode('utf-8'))


class AssetLink():
    def __init__(self, topdesk=None, sourceid=None, targetid=None,
                 linktype='child', linkid=None, capabilityid=None):
        self.topdesk = topdesk
        self.sourceid = sourceid
        self.targetid = targetid
        self.linktype = linktype
        self.linkid = linkid
        self.capabilityid = capabilityid

    def create(self):
        """Create AssetLink between Assets.

        Create an Assetlink between the Asset with the specified sourceid and
        the Asset with the specified targetid. The AssetLink will be created
        with either the specified capabilityid and either the specified
        capabilityid or (when this is not specified) the specified linktype
        (defaults to 'child').

        :returns:
            The created AssetLink object.

        :raises:
            BadRequestError if AssetLink could not be created.

        """
        endpoint = '/tas/api/assetmgmt/assetLinks'
        if self.capabilityid:
            payload = {
                    "sourceId": self.sourceid,
                    "targetId": self.targetid,
                    "capabilityId": self.capabilityid
                    }
        else:
            payload = {
                    "sourceId": self.sourceid,
                    "targetId": self.targetid,
                    "type": self.linktype,
                    }
        response = self.topdesk.request(endpoint, 'post', payload)

        if response.ok:
            self.data = json.loads(response.content.decode('utf-8'))
            self.linkid = self.data['linkId']
            self.linktype = self.data['linkType']
            return
        else:
            logger.error("request %d error: '%s'",
                         response.status_code,
                         response.content.decode('utf-8'))
            raise BadRequestError("Could not create AssetLink")

    def delete(self):
        """Delete AssetLink between Assets.

        Based on the available info an Assetlink between Assets will be deleted.
        If a linkid is specified, the linkid will be used to delete the AssetLink.
        If the linkid is not available, the specified sourceid, targetid and
        either the capabilityid or (when this is not specified) the specified
        linktype (defaults to 'child') will be used to identify the AssetLink to
        be deleted.

        :returns:
            Nothing because the AssetLink object is deleted.

        :raises:
            NoAssetLinkError if no AssetLink could be found.

        """
        if not self.linkid:
            assetlinks = self.topdesk.listAssetLinks(unid=self.sourceid)
            if self.capabilityid:
                for a in assetlinks:
                    if (self.targetid == a['assetId'] and
                    'capabilityId' in a and
                    self.capabilityid == a['capabilityId']):
                        self.linkid = a['linkId']
                        break
            else:
                for a in assetlinks:
                    if (self.targetid == a['assetId'] and
                    self.linktype == a['linkType']):
                        self.linkid = a['linkId']
                        break
        if self.linkid:
            endpoint = '/tas/api/assetmgmt/assetLinks' + '/' + self.linkid
            response = self.topdesk.request(endpoint, 'delete')
        else:
            message = ("No link found for asset %s" % self.sourceid)
            logger.debug(message)
            raise NoAssetLinkError(message)

        if response.ok:
            return None
        else:
            logger.error("request %d error: '%s'",
                         response.status_code,
                         response.content.decode('utf-8'))
            raise BadRequestError("Could not delete AssetLink")


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class BadRequestError(Error):
    """Exception raised for generally bad requests"""

    def __init__(self, message):
        self.message = message


class NoAssetLinkError(Error):
    """Exception raised for errors when no AssetLink could be found"""

    def __init__(self, message):
        self.message = message


def login(url=None, user=None, secret=None):
    t = Topdesk(url.strip('/'), user, secret)
    return t
