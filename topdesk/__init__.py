# -*- coding: utf-8 -*-
"""
topdesk.py
==========
See https://github.com/MakeExpose/topdesk.py for documentation.
:copyright: (c) 2018 by Naturalis Biodiversity Center
:license: LGPL, see LICENSE for more details
"""

from .__about__ import (
    __package_name__, __title__, __author__, __author_email__,
    __license__, __copyright__, __version__, __version_info__,
    __url__,
)

from .topdesk import login
