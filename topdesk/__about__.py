"""The module that holds much of the metadata about topdesk.py."""
__package_name__ = 'topdesk.py'
__title__ = 'topdesk'
__author__ = 'Naturalis Biodiversity Center'
__author_email__ = 'support@naturalis.nl'
__license__ = 'LGPL'
__copyright__ = 'Copyright 2018 Naturalis Biodiversity Center'
__version__ = '0.0.1'
__version_info__ = tuple(int(i) for i in __version__.split('.') if i.isdigit())
__url__ = 'https://github.com/MakeExpose/topdesk.py'

__all__ = (
    '__package_name__', '__title__', '__author__', '__author_email__',
    '__license__', '__copyright__', '__version__', '__version_info__',
    '__url__',
)
