import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="topdesk",
    version="0.0.1",
    author="Naturalis Biodiversity Center",
    author_email="support@naturalis.nl",
    description="Wrapper for cherry picked TOPdesk API functions",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/makeexpose/topdesk.py",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: LGPL License",
        "Operating System :: OS Independent",
    ),
)
